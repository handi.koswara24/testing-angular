import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AlertButtonComponent } from './alert-button.component';

import { MessageService } from '../message.service';
import { of } from 'rxjs/observable/of';


describe('AlertButtonComponent', () => {
  let component: AlertButtonComponent;
  let fixture: ComponentFixture<AlertButtonComponent> //test environtment;
  let de: DebugElement //render HTML

  let serviceStub: any;

  beforeEach(async(() => {

    serviceStub = {
      getContent: () => of('you have been warned'),
    };

    TestBed.configureTestingModule({
      declarations: [AlertButtonComponent],
      imports: [HttpClientTestingModule]
      // providers: [{ provide: MessageService, useValue: serviceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertButtonComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a message with `warn`', () => {
    expect(component.content).toContain('warn');
  });

  it('should have a cost greater than 2', () => {
    expect(component.cost).toBeGreaterThan(200);
  });

  it('should have an H1 tag of `Alert Button`', () => {
    expect(de.query(By.css('h1')).nativeElement.innerText).toBe('Alert Button');
  });

  it('should toggle the message boolean', () => {
    expect(component.hideContent).toBeTruthy();
    component.toggle();
    expect(component.hideContent).toBeFalsy();
  });

  it('should toggle the message boolean asynchronously', fakeAsync(() => {
    expect(component.hideContent).toBeTruthy();
    component.toggleAsync();
    tick(500);
    expect(component.hideContent).toBeFalsy();
  }));

  // it('should have message content defined from an observable', () => {
  //   component.content.subscribe(content => {
  //     expect(content).toBeDefined();
  //   })
  // });
});
