import { Component, OnInit } from '@angular/core';
import { timer, Observable } from 'rxjs';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-alert-button',
  templateUrl: './alert-button.component.html',
  styleUrls: ['./alert-button.component.css']
})
export class AlertButtonComponent implements OnInit {

  content = "you have been warned";
  // content: Observable<any>;
  hideContent = true;
  cost = 123;

  data: any;


  constructor(private messageService: MessageService) {
    // this.messageService.getData().subscribe(data => {
    //   this.content = data.message;
    // })
  }

  ngOnInit() {
  }

  toggle() {
    this.hideContent = !this.hideContent;
  }

  toggleAsync() {
    timer(500).subscribe(() => {
      this.toggle();
    });
  }

}
