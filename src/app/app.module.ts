import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AlertButtonComponent } from './alert-button/alert-button.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HandiComponent } from './handi/handi.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertButtonComponent,
    HandiComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientTestingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
