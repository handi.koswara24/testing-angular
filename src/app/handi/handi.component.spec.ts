import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandiComponent } from './handi.component';

describe('HandiComponent', () => {
  let component: HandiComponent;
  let fixture: ComponentFixture<HandiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
